<?php
session_start();

define('CLIENT_ID', 'YOUR CLIENT ID HERE');
define('CLIENT_SECRET', 'YOUR CLIENT SECRET HERE');
define('REDIRECT_URI', 'YOUR REDIRECT URI HERE');

define('LIVE_STATE', 'live_state');

function generate_state()
{
  return $_SESSION[LIVE_STATE] = md5(uniqid(LIVE_STATE, TRUE));
}

function validate_state($state)
{
  if (!array_key_exists(LIVE_STATE, $_SESSION)) return FALSE;
  return $state == $_SESSION[LIVE_STATE];
}

function goto_live()
{
  $params = http_build_query(array(
    'response_type' => 'code',
    'client_id' => CLIENT_ID,
    'redirect_uri' => REDIRECT_URI,
    'scope' => 'wl.signin wl.emails',
    'state' => generate_state(),
  ));
  header("Location: https://login.live.com/oauth20_authorize.srf?{$params}");
  exit();
}

if (!array_key_exists('code', $_GET)) goto_live();
if (!array_key_exists('state', $_GET)) goto_live();
if (!validate_state($_GET['state'])) goto_live();

$url = 'https://login.live.com/oauth20_token.srf';
$data = http_build_query(array(
  'code' => $_GET['code'],
  'client_id' => CLIENT_ID,
  'client_secret' => CLIENT_SECRET,
  'redirect_uri' => REDIRECT_URI,
  'grant_type' => 'authorization_code',
));
$context = stream_context_create(array('http' => array(
  'method' => 'POST',
  'header' => "Content-Type: application/x-www-form-urlencoded\r\n" .
              'Content-Length: ' . strlen($data) . "\r\n" .
              'Accept: application/json',
  'content' => $data,
)));
$json = json_decode(file_get_contents($url, FALSE, $context), TRUE);
$access_token = $json['access_token'];

$url = "https://apis.live.net/v5.0/me?access_token={$access_token}";
$userinfo = json_decode(file_get_contents($url), TRUE);

print('<pre>');
print_r($userinfo);

