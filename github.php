<?php
session_start();

define('CLIENT_ID', 'YOUR CLIENT ID HERE');
define('CLIENT_SECRET', 'YOUR CLIENT SECRET HERE');

define('GITHUB_STATE', 'github_state');

function generate_state()
{
  return $_SESSION[GITHUB_STATE] = md5(uniqid(GITHUB_STATE, TRUE));
}

function validate_state($state)
{
  if (!array_key_exists(GITHUB_STATE, $_SESSION)) return FALSE;
  return $state == $_SESSION[GITHUB_STATE];
}

function goto_github()
{
  header('Location: https://github.com/login/oauth/authorize?client_id=' . CLIENT_ID . '&scope=user&state=' . generate_state());
  exit();
}

if (!array_key_exists('code', $_GET)) goto_github();
if (!array_key_exists('state', $_GET)) goto_github();
if (!validate_state($_GET['state'])) goto_github();

$url = 'https://github.com/login/oauth/access_token';
$data = http_build_query(array(
  'client_id' => CLIENT_ID,
  'client_secret' => CLIENT_SECRET,
  'code' => $_GET['code'],
  'state' => $_GET['state'],
));
$context = stream_context_create(array('http' => array(
  'method' => 'POST',
  'header' => "Content-Type: application/x-www-form-urlencoded\r\n" .
              'Content-Length: ' . strlen($data) . "\r\n" .
              'Accept: application/json',
  'content' => $data,
)));
$json = json_decode(file_get_contents($url, FALSE, $context), TRUE);
$access_token = $json['access_token'];

$url = "https://api.github.com/user/emails?access_token={$access_token}";
$emails = json_decode(file_get_contents($url), TRUE);

print('<pre>');
print_r($emails);

